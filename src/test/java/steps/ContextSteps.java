package steps;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.JavascriptExecutor;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import utility.BrowserFactory;
import utility.RespositoryParser;

public class ContextSteps extends BrowserFactory {
	private static Logger Log = LogManager.getLogger(ContextSteps.class.getName());
	protected ThreadLocal<String[]> threadShareArray = null;
	private boolean initialized = false;
	private static boolean dunit = false;

	@Before
	public void setUp() throws Exception {
		if (!initialized) {
			if (getWebDriver() == null) {
				startWebDriver();
			}
			String propertyFileName = System.getProperty("propertyFile") == null ? RespositoryParser.getSystemValue("propertyFile") : System.getProperty("propertyFile");
			RespositoryParser.setupDataFile(propertyFileName);
			initialized = true;
			threadShareArray = new ThreadLocal <String[]>();
			if (new File("target/surefire-reports/").exists() == false) {
				new File("target/surefire-reports/").mkdirs(); // Insure directory is there
			}
		}
	}
	
	public String[] getShareArrayValue() {
		return threadShareArray.get();
	}
	
	public void setShareArrayValue(String[] shareValue) {
		threadShareArray.set(shareValue);
	}
	
	@After
    public void after(Scenario scenario) {
		try {
			if (scenario.isFailed()) {
				String javascript = "return arguments[0].innerHTML";
				JavascriptExecutor js = (JavascriptExecutor) getWebDriver();
				String pageSource= (String) js.executeScript(javascript, getWebDriver().findElement(By.tagName("html")));
				String fileName = scenario.getName() + "-" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()).toString();
                FileOutputStream out = new FileOutputStream("target/surefire-reports/screenshot-" + fileName + ".png");
                out.write(((TakesScreenshot) getWebDriver()).getScreenshotAs(OutputType.BYTES));
                out.close();
                File pageSourceFile = new File("target/surefire-reports/pageSource-" + fileName + ".html");
                
                BufferedWriter bw = new BufferedWriter(new FileWriter(pageSourceFile));
                bw.write(pageSource);
                bw.close();
                
                final byte[] screenshot = ((TakesScreenshot) getWebDriver()).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png");
                scenario.write(pageSource);
			}
		} catch (IOException e) {
			
		} finally {
			getWebDriver().manage().deleteAllCookies();
			if(!dunit) {
	            Runtime.getRuntime().addShutdownHook(new Thread() {
	                public void run() {
	                    stopWebDriver();
	                }
	            });
	            dunit = true;
	        }
		}
    }
}
