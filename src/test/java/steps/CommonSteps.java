package steps;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import common.CommonLib;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import utility.RespositoryParser;

public class CommonSteps extends CommonLib {
	private static Logger Log = LogManager.getLogger(CommonSteps.class.getName());
	
	
	public CommonSteps (ContextSteps cs) {
		CommonSteps.cs = cs;
	
	}
	
	@Given("^I navigate to \"([^\"]*)\"$")
	public void iNavigateTo(String pageUrl) throws Throwable {
		Log.info("going to page " + pageUrl);
		cs.getWebDriver().get(RespositoryParser.getDataValue(pageUrl));
	}
	
	@Then("^I navigate to baseUrl \"([^\"]*)\"$")
	public void iNavigateToBaseUrlWith(String pageUrl) throws Throwable {
		pageUrl = System.getProperty("baseUrl") + RespositoryParser.getDataValue(pageUrl);
		Log.info("going to page" + pageUrl );
		cs.getWebDriver().get(pageUrl);
	}
}
