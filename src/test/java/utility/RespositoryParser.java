package utility;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;

public class RespositoryParser {
	private static InputStreamReader stream;
	private static String RepositoryFile;
	private static Logger Log = LogManager.getLogger(RespositoryParser.class.getName());
	static Properties propertyFile = new Properties();
	static Properties locatorFile = new Properties();

	public static void setupSystemFile(String fileName) throws IOException {
		RepositoryFile = fileName;
		stream = new InputStreamReader(new FileInputStream(RepositoryFile), "UTF-8");
		propertyFile.load(stream);
	}
	
	public static void setupDataFile(String fileName) throws IOException {
		RepositoryFile = fileName;
		stream = new InputStreamReader(new FileInputStream(RepositoryFile), "UTF-8");
		locatorFile.load(stream);
	}
	
	public static String getSystemValue(String propertyKey) {
		String propertyValue = propertyFile.getProperty(propertyKey);
		Log.info(propertyKey + " " + propertyValue);
		return propertyValue;
	}
	
	public static String getDataValue(String locatorKey) throws UnsupportedEncodingException {
		String dataValue = locatorKey.equals("baseUrl") ? System.getProperty("baseUrl") : null;
		if (dataValue == null) {
			dataValue = getSystemValue(locatorKey) ;
		}
		if (dataValue != null && !dataValue.isEmpty()) {
			return dataValue;
		}
		try {
			byte[] dataVaueByte = locatorFile.getProperty(locatorKey).getBytes("UTF-8");
			dataValue = new String(dataVaueByte, "UTF-8");
			Log.info(locatorKey + " " + dataValue);
			return dataValue;
		} catch (NullPointerException e) {
			return locatorKey;
		}
	}
	
//	private enum LocatorType {
//		Id, Name, CssSelector, LinkText, PartialLinkText, TagName, Xpath;
//	}
//	
//	public static By getLocatorByType(String locatorValue, String locatorTypeString) {
//		String tempLocatorValue = locatorFile.getProperty(locatorValue);
//		LocatorType locatorType = LocatorType.valueOf(locatorTypeString);
//		locatorValue = tempLocatorValue == null ? locatorValue : tempLocatorValue;
//		Log.info("Locator Type: " + locatorTypeString + " Locator Value: " + locatorValue);
//		By locator = null;
//		
//		switch(locatorType)
//		{
//		case Id:
//			locator = By.id(locatorValue);
//			break;
//		case Name:
//			locator = By.name(locatorValue);
//			break;
//		case CssSelector:
//			locator = By.cssSelector(locatorValue);
//			break;
//		case LinkText:
//			locator = By.linkText(locatorValue);
//			break;
//		case PartialLinkText:
//			locator = By.partialLinkText(locatorValue);
//			break;
//		case TagName:
//			locator = By.tagName(locatorValue);
//			break;
//		case Xpath:
//			locator = By.xpath(locatorValue);
//			break;
//		}
//		Log.info("Locator By Value: " + locator.toString());
//		return locator;
//	}
}
