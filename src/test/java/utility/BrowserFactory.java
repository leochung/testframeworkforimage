package utility;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BrowserFactory {
	private static Logger Log = LogManager.getLogger(BrowserFactory.class.getName());
	protected static Map<String, RemoteWebDriver> mapDrivers = null;
	private static WebDriverWait wait;
	private static String userAgentValue = "";
	private static String browser;
	
	public void stopWebDriver() {
		getWebDriver().quit();
		mapDrivers.remove(browser);
	}

	public RemoteWebDriver getWebDriver() {
		if (mapDrivers == null || mapDrivers.get(browser) == null) {
			return null;
		} 
		return mapDrivers.get(browser);
	}
	
	public WebDriverWait getWebDriverWait() {
		return wait;
	}

	public void startWebDriver() throws IOException {
		RespositoryParser.setupSystemFile("System.properties");
		String seleniumGridURL = System.getProperty("seleniumGridURL") == null ? RespositoryParser.getSystemValue("seleniumGridURL") : System.getProperty("seleniumGridURL");
		browser = System.getProperty("browser") == null ? RespositoryParser.getSystemValue("browser") : System.getProperty("browser");
		
		mapDrivers = new HashMap<String, RemoteWebDriver>();
		mapDrivers.put(browser, new RemoteWebDriver(new URL(seleniumGridURL), getBrowserCapabilities(browser)));
		getWebDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		getWebDriver().manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		getWebDriver().manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
		if (userAgentValue == "" || userAgentValue == null) {
			getWebDriver().manage().window().maximize();
		} else {
			getWebDriver().manage().window().setSize(new Dimension(375, 667));
		}
		wait = new WebDriverWait(getWebDriver(), 6);
	}

	private DesiredCapabilities getBrowserCapabilities(String browserType) {
		BrowserName browserName = BrowserName.valueOf(browserType);
		String userAgent = System.getProperty("userAgent") == null ? RespositoryParser.getSystemValue("userAgent")
				: System.getProperty("userAgent");
		userAgentValue = userAgent == "" || userAgent == null ? "" : RespositoryParser.getSystemValue(userAgent);
		switch (browserName) {
		case firefox:
			Log.info("Opening firefox driver");
			return getBrowserWithProfile(new FirefoxProfile());
		case chrome:
			Log.info("Opening chrome driver");
			return getBrowserWithProfile(new ChromeOptions());
		case safari:
			Log.info("Opening safari driver");
			return DesiredCapabilities.safari();
		case IE:
			Log.info("Opening IE driver");
			return DesiredCapabilities.internetExplorer();
		default:
			Log.info("browser : " + browserType + " is invalid, Launching Firefox as browser of choice..");
			return DesiredCapabilities.firefox();
		}
	}

	private enum BrowserName {
		firefox, chrome, IE, safari;
	}

	private DesiredCapabilities getBrowserWithProfile(Object profile) {
		DesiredCapabilities dc = null;
		if (userAgentValue != "" && userAgentValue != null) {
			if (profile instanceof FirefoxProfile) {
				((FirefoxProfile) profile).setPreference("general.useragent.override", userAgentValue);
				dc = DesiredCapabilities.firefox();
				dc.setCapability(FirefoxDriver.PROFILE, profile);
			} else if (profile instanceof ChromeOptions) {
				((ChromeOptions) profile).addArguments("--user-agent=" + userAgentValue);
				dc = DesiredCapabilities.chrome();
				dc.setCapability(ChromeOptions.CAPABILITY, profile);
			}
		} else {
			if (profile instanceof FirefoxProfile) {
				dc = DesiredCapabilities.firefox();
			} else if (profile instanceof ChromeOptions) {
				((ChromeOptions) profile).addArguments("disable-infobars");
				Map<String, Object> prefs = new HashMap<String, Object>();
			    prefs.put("credentials_enable_service", false);
			    prefs.put("profile.password_manager_enabled", false);
			    ((ChromeOptions) profile).setExperimentalOption("prefs", prefs);
				dc = DesiredCapabilities.chrome();
				dc.setCapability(ChromeOptions.CAPABILITY, profile);
			}
		}
		return dc;
	}
}
